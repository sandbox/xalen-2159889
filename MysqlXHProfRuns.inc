<?php

/**
 * @file
 * Definition of MongodbXHProfRuns.
 */

/**
 * Defines a MongoDB storage backend for XHProf.
 */
class MysqlXHProfRuns implements XHProfRunsInterface {

  private $dir = '';

  private function gen_run_id($type) {
    return uniqid();
  }

  private function file_name($run_id, $type) {
    $file = "$run_id.$type";

    if (!empty($this->dir)) {
      $file = $this->dir . "/" . $file;
    }
    return $file;
  }

  public function __construct($dir = NULL) {
  }

  /**
  * This function gets runs based on passed parameters, column data as key, value as the value. Values
  * are escaped automatically. You may also pass limit, order by, group by, or "where" to add those values,
  * all of which are used as is, no escaping.
  *
  * @param array $stats Criteria by which to select columns
  * @return resource
  */
  public function getRuns($stats, $limit = 50, $skip = 0) {
    $sort = isset($_GET['order']) ? $_GET['order'] : 'date';
    $sort_direction_str = isset($_GET['sort']) ? $_GET['sort'] : 'desc';
    $sort_direction = array(
      'asc' => 1,
      'desc' => -1,
    );

    $result = array(); // @todo - Get the rows from thy xhprof table

    $runs = array();
    foreach ($result as $id => $run) {
      $run['run_id'] = $id;
      $runs[] = $run;
    }

    return $runs;
  }

  public function getCount() {

    // @todo - Count records in xhprof
    return $count;
  }

  public function get_run($run_id, $type, &$run_desc) {
    $run_desc = "XHProf Run (Namespace=$type)";

    // @todo - Get a single run
    $run = array();

    return $run['run_data'];
  }

  public function save_run($xhprof_data, $type, $run_id = NULL) {
    if ($run_id === NULL) {
      $run_id = $this->gen_run_id($type);
    }

    $entry['path'] = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
    $entry['servername'] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';

    // @todo - Save the run to xhprof table

    return $run_id;
  }
}
